#pragma once

typedef struct SkyData {
  float time;
  float cirrus;
  float cumulus;
  float3 sun_dir;
} SkyData;
